variable domain {}
variable unit {}
variable environment {}
variable project {}
variable name {}
variable allowed_groups {
  type = list(string)
}
