resource aws_s3_bucket bucket {
  bucket = local.bucket_name
  acl    = "private"

  versioning {
    enabled = true
  }

  tags = local.tags
}

data aws_iam_policy_document bucket_access {
  statement {
    effect = "Allow"

    actions = [
      "s3:*",
    ]

    resources = [
      aws_s3_bucket.bucket.arn,
      format("%s/*", aws_s3_bucket.bucket.arn),
    ]
  }
}

resource aws_iam_policy bucket_access {
  name        = local.access_policy_name
  path        = local.iam_path
  description = format("Provides access to the %s s3 bucket", local.bucket_name)

  policy = data.aws_iam_policy_document.bucket_access.json
}

resource aws_iam_group_policy_attachment bucket_access {
  count = length(var.allowed_groups)

  policy_arn = aws_iam_policy.bucket_access.arn
  group      = element(var.allowed_groups, count.index)
}
